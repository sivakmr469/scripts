#!/bin/bash
ID=$(id -u)

VALIDATE(){
    if [ $? -ne 0 ]; then
        echo -e "\e[31m $2...ERROR\e[0m"
        exit 1
    else
        echo -e "\e[32m $2...SUCCESS\e[0m"
    fi
}

if [ $ID != 0 ]; then
    echo "Please run this script as root user"
    exit 1
fi
##create group
id centos &> /dev/null
if [ $? -eq 0 ] ; then
    groupadd -g 3000 docker
    usermod -a -G docker centos
fi

yum install -y yum-utils device-mapper-persistent-data lvm2 &> /dev/null
VALIDATE $? "utils installation"
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo &> /dev/null
VALIDATE $? "adding repo"
yum install docker-ce docker-ce-cli containerd.io -y &> /dev/null
VALIDATE $? "dokcer installation"
systemctl start docker
systemctl enable docker

echo -e "\e[33m You need to logout and login again and run the following command.\e[0m"
echo -e "\e[32m docker ps"