#!/bin/bash
LOG=current.log
ID=$(id -u)
R="\e[31m"
G="\e[32m"
N="\e[0m"
VALIDATE(){

    if [ $? -ne 0 ]; then
        echo -e "$2...$R FAILURE $N"
        exit 1
    else
        echo -e "$2...$G SUCCESS $N"
    fi
}

if [ $ID != 0 ]; then
    echo -e "$R Please run this script as rooot user $N" 
fi

############Installing Containerd and Docker

yum install yum-utils device-mapper-persistent-data lvm2 -y &> $LOG

VALIDATE $? "Installing yum utills"

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo &> $LOG

VALIDATE $? "Adding Docker repo"

yum update -y &> $LOG && yum install containerd.io-1.2.10 docker-ce-19.03.4 docker-ce-cli-19.03.4 -y &> $LOG

VALIDATE $? "Installing docker and containerd"

mkdir /etc/docker

VALIDATE $? "Creating docker dir in /etc"

# Setup daemon.
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

mkdir -p /etc/systemd/system/docker.service.d

VALIDATE $? "Creating docker service.d"

# Restart docker.
systemctl daemon-reload
systemctl restart docker

VALIDATE $? "Starting Docker"


################ Installing Kubeadm #############

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config &> $LOG

VALIDATE $? "Disabling SELINUX"

yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes -y &> $LOG

VALIDATE $? "Installing kubeadm and kubectl"

systemctl enable --now kubelet

VALIDATE $? "Enabled kubelet"

systemctl disable firewalld

systemctl stop firewalld

VALIDATE $? "Disabled firewall"

cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sysctl --system

systemctl daemon-reload
systemctl restart kubelet

VALIDATE $? "Started Kubelet"

systemctl enable docker

sed -i '1inet.ipv4.ip_forward = 1' /etc/sysctl.conf
sysctl -p &> $LOG

VALIDATE $? "set the ip table forward to 1 in /etc/sysctl.conf"

echo "Now you can run kubectl join command to join with master"

